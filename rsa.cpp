#include "rsa.h"
#include <random>
#include <time.h>
#include <iostream>
#include <math.h>

std::default_random_engine re(time(nullptr));

std::uniform_int_distribution<int> distrib{100, 80000};

long long findN(long long& p, long long& q) {
    p = 0;
    q = 0;
    int _resp = 0;
    int _resq = 0;

    while(_resp != 1)
    {
        p = distrib(re);
        _resp = premier(p);
    }
    std::cout << "p : " << p << std::endl;

    while(_resq != 1)
    {
        q = distrib(re);
        _resq = premier(q);
    }
    std::cout << "q : " << q << std::endl;

    std::cout << "N : " << p*q << std::endl;

    return p*q;
}

int premier(long long n)
{
    long long int d;

    if (n % 2 == 0)
      return (n == 2);
    for (d = 3; d * d <= n; d = d + 2)
      if (n % d == 0)
        return 0;
    return 1;
}

long long PGCD(long long a, long long b)
{
    while(a != b)
    {
        if(a > b)
            a = a - b;
        else {
            b = b - a;
        }
    }
    return a;
}

long long findE(long long p, long long q)
{
    long long phiden = (p-1) * (q-1);
    long long PGCD1 = 0;
    int cpt = 0;
    long long e = 0;

    while(PGCD1 != 1)
    {
        cpt = 0;
        while(cpt == 0)
        {
            if((p < e) && (q < e) && (e < phiden))
                cpt = 1;
            e += 1;
        }
        static int num = 0;
        std::cout << "Essai numero " << num << std::endl;
        num++;
        std::cout << "e : " << e << std::endl;
        std::cout << "phiden : " << phiden << std::endl;
        PGCD1 = PGCD(e, phiden);
    }
    return e;
}

std::string chiffrer(std::string word)
{
    std::string word_crypt = "";
    for(char c : word)
    {
        word_crypt += c+" ";
    }
}
