#ifndef RSA_H
#define RSA_H

#include <string>

long long int findN(long long&, long long&);
long long findE(long long, long long);
int premier(long long);
long long PGCD(long long, long long);

std::string chiffrer(std::string);

#endif // RSA_H
